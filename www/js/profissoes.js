$('#profissoesPage').bind('pageinit', function(event) {
    var db = window.openDatabase("guiapro", "1.0", "Guia de Profissional", 200000);
    db.transaction(listProfissoes, errorListProfissoes);
});

/**
 * Seleciona a lista de profissoes
 * @param {type} tx
 * @returns {undefined}
 */
function listProfissoes(tx) {
//    tx.executeSql('DROP TABLE IF EXISTS profissoes');
    tx.executeSql('CREATE TABLE IF NOT EXISTS profissoes (id unique, profissao)');
    tx.executeSql('SELECT * FROM profissoes ORDER BY profissao ASC', [], listProfissoesSuccess, errorListProfissoes);
}

/**
 * Callback para quando houver um erro
 * @param {type} err
 * @returns {undefined}
 * @todo: criar tela de erro
 */
function errorListProfissoes(err) {
    //criar tela
    alert("Error processing SQL: " + err);
}

/**
 * Callback que processará a lista de profissoes e criará a lista 
 *  
 * @param {type} tx
 * @param {type} results
 * @returns {undefined}
 */
function listProfissoesSuccess(tx, results) {
    var len = results.rows.length;
    if (len > 0) {
//        alert('com DB');
        var arrayProf = [];
        for (var i = 0; i < len; i++) {
            arrayProf[results.rows.item(i).id] = results.rows.item(i).profissao;
        }
        popularProfissoesList(arrayProf);
    }
    else {
//        alert('sem db');
        apiGetProfissao();
    }
}

/**
 * Callback para quando houver um resultado com sucesso
 * @returns {undefined}
 */
function successCB() {
    $('#x').html("success!");
}

/**
 * Get Json
 */
function apiGetProfissao() {
    var arrayProf = [];
    var jsonProf = urlProfissoes;
    $.ajax({
        url: jsonProf,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {
            //nada
        },
        success: function(json) {
            //inserir as profissoes no DB
            $.each(json, function(i, item) {
                arrayProf[item.id] = item.profissao;
            });
            popularProfissoesList(arrayProf);
            doInsert(arrayProf);
        }
    });
}


/**
 * * Cria a listagem de profissoes
 * @param {type} results
 * @returns {undefined}
 */
function popularProfissoesList(results) {
    var len = results.length;
    if (len > 0) {
        for (var key in results) {
            $('#lst_profissoes').append('<li class="ui-first-child ui-last-child"><a class="ui-btn ui-btn-icon-right ui-icon-carat-r" href="javascript:void(0)" onclick="$.mobile.changePage(\'profissionais.html?id=' + key + '|' + results[key] + '\', { transition: \'slideup\'});">' + results[key] + '</a></li>');
        }
        $('#lista_profissoes').show();
    }
}


/**
 * Insere os dados no DB
 * @param {type} data
 * @returns {undefined}
 */
function doInsert(data) {
    var db = window.openDatabase("guiapro", "1.0", "Guia de Profissional", 200000);
    var texto = [];
    for (var key in data) {
        texto.push("(" + key + ", '" + data[key] + "')");
    }
    var sql = "INSERT INTO profissoes (id, profissao) VALUES " + texto.join(',');
    db.transaction(function(tx) {
        tx.executeSql(
                sql,
                [],
                insertSuccess,
                insertFail
                );
    });
}

/**
 * Callback para quando terminar de inserir os dados
 * @returns {undefined}
 */
function insertSuccess() {
    //alert('inserido');
}

/**
 * Callback para erros
 * @param {type} err
 * @returns {undefined}
 */
function insertFail(err) {
    //alert('insertFail, err.message: ' + err.message);
}


